package benchr

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/prosp3r/benchr/blog"
)

//Bconfig will hold the structure for benchr configuration from json
type Bconfig struct {
	Webport     string `json:"webport"`
	Loglocation string `json:"loglocation"`
	Version     string `json:"version"`
}

//Bconfigx will hold an instance of Benchr config
var Bconfigx []Bconfig

var upgrader = websocket.Upgrader{
	ReadBufferSize:    1024,
	WriteBufferSize:   1024,
	EnableCompression: true,
}

//Setconfig will load the settings from persistence medium or system system variable
func Setconfig() ([]Bconfig, error) {
	configFile := "config.json"
	b, err := os.Open(configFile)
	if err != nil {
		_ = blog.Botlog("Failed opening config types file: " + err.Error())
		return nil, err
	}

	defer b.Close()

	bt, _ := ioutil.ReadAll(b)
	if err = json.Unmarshal(bt, &Bconfigx); err != nil {
		_ = blog.Botlog("Error Unmarshaling config types : " + err.Error())
		return nil, err
	}

	return Bconfigx, nil
}

//Benchmark will initiate all benchr needed functions and files
func Benchmark() error {
	Bconfigx = nil
	//PRELOAD FROM DB FOR SPEED
	Setconfig() //Load config files once

	//users.Setusers()
	//users.Setsubscribers()

	//
	//DEFINE API END POINTS
	http.HandleFunc("/", servehome)
	http.HandleFunc("/assets/css/bootstrap.min.css", servebootstrapcss)
	http.HandleFunc("/assets/javascript/jquery.min.js", servejqueryjs)
	http.HandleFunc("/assets/javascript/bootstrap.min.js", servebootstrapjs)
	http.HandleFunc("/assets/css/bootstrap.min.css.map", parseMap)
	http.HandleFunc("/ws", socketConn)
	//END API ENDPOINTS DEFINITION
	//port := Bconfigx.Webport
	http.ListenAndServe(":2019", nil)
	//log.Fatal(http.ListenAndServe(":2210", router))
	return nil
}

//socketConn will initiate the web socket connection
func socketConn(w http.ResponseWriter, r *http.Request) {
	var conn, _ = upgrader.Upgrade(w, r, nil)
	go func(conn *websocket.Conn) {
		//read messages from client
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				conn.Close()
			}
		}
	}(conn)

	//write side
	go func(conn *websocket.Conn) {
		ch := time.Tick(5 * time.Second)
		for range ch {
			//SEND RESPONSE BACK AFTER DOING SOMETHING WITH THE DATA
			//conn.WriteMessage(myType, msg)
			//sending json object
			//type myjs struct{} //temporary declaration
			//users.GetSubscribers()
			message := blog.FormatErrorMessage("Connected to server", "Error")
			//jsonObject := myjs{}
			fmt.Println(message)
			jsonObject := message
			conn.WriteJSON(jsonObject)
		}
	}(conn)
}

//MeasureTime measures the time and saves the benchmark log to log file
func MeasureTime(funcName string) func() {
	//fmt.Println("Here now ooo")
	start := time.Now()
	return func() {
		//fmt.Printf("Time taken by %s function is %v \n", funcName, time.Since(start))
		lapse := time.Since(start)                        //convert to seconds
		lapseString := fmt.Sprintf("%f", lapse.Seconds()) //convert float to string
		_ = blog.Benchlog("Time taken by "+funcName+" function is "+lapseString+" seconds", start, lapse)
		//fmt.Println(msg)
	}
}

//servejqueryjs will return statis js files
func servejqueryjs(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/javascript/jquery.min.js")
}

//./assets/javascript/bootstrap.min.js
func servebootstrapjs(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/javascript/bootstrap.min.js")
}

//servebootstrap will serve the static bootstrap file
func servebootstrapcss(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/css/bootstrap.min.css")
}

func parseMap(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "assets/css/bootstrap.min.css.map")
}

//servehome will return the static home page html file
func servehome(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "index.html")
}
