package blog

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

//ErrorMessage structure
type ErrorMessage struct {
	//2019/11/02 09:58:16 Error Unmarshaling config types : json: cannot unmarshal object into Go value of type []benchr.Bconfig
	Eventid     string `json:"eventid"`
	Timeofevent string `json:"timeofevent,omitempty"`
	Typeofevent string `json:"typeofevent"`
	Message     string `json:"message"`
}

var emessage []ErrorMessage

//FormatErrorMessage returns message formated in json string for storing in log files
func FormatErrorMessage(message, messageType string) string {
	xtime := time.Now()
	ftime := xtime.Format("2006-01-02T15:04:05.999999-07:00")
	//messageType := "Errorlog"
	//jsonmessage := ErrorMessage{GetGUID(6, "ALPHANUMERIC"), ftime, messageType, message}
	jsonmessage := map[string]string{"reportid": GetGUID(6, "ALPHANUMERIC"), "reporttime": ftime, "messagetype": messageType, "message": message}
	slcB, _ := json.Marshal(jsonmessage)
	jsonmessagex := string(slcB)

	return jsonmessagex
}

//Botlog writes message to a log file in the log directory
func Botlog(message string) bool {
	messageType := "Errorlog"
	fmessage := FormatErrorMessage(message, messageType)
	logfile := "./boterror.log"
	f, err := os.OpenFile(logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
		//fmt.Println(err.Error())
		//Pigeonlog(err.Error())
		return false
	}
	defer f.Close()

	log.SetOutput(f)
	log.Println(fmessage)

	return true
}

//Benchlog writes message to a log file in the log directory
func Benchlog(message string, startTime time.Time, timeTaken time.Duration) bool {
	fmt.Println("Here now ooo")
	messageType := "Benchmark"
	fmessage := FormatErrorMessage(message, messageType)

	logfile := "bechr.log"
	f, err := os.OpenFile(logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)

		return false
	}
	defer f.Close()

	fmt.Println(fmessage)

	log.SetOutput(f)
	log.Println(fmessage)

	return true
}

//GetGUID Returns a random (alphabetic, alphanumeric, numeric) generated string of given length
func GetGUID(length int64, resultType string) string {

	alphax := make([]string, 0)
	alphax = append(alphax, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

	nums := make([]string, 0)
	nums = append(nums, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9")
	//PREVENT SAME NUMBER GENERATION ERROR WITH RAND.INTN
	rand.Seed(time.Now().UnixNano())

	if length > 0 {
		if resultType == "ALPHANUMERIC" {
			resultx := ""

			alphaNum := append(nums, alphax...) //join both alphabetic and numeric slices
			//fmt.Println(alphaNum)
			for x := int64(0); x < length; x++ {
				maxi := len(alphaNum) //maximum length of new slice
				tag := int64(rand.Intn(maxi))
				resultx = resultx + alphaNum[tag]
			}
			return resultx
		}

		if resultType == "NUMERIC" {
			resultx := ""
			for x := int64(0); x < length; x++ {
				tag := int64(rand.Intn(10))
				resultx = resultx + nums[tag]
			}
			return resultx
		}

		if resultType == "ALPHABETIC" {
			resultx := ""
			for x := int64(0); x < length; x++ {
				tag := int64(rand.Intn(10))
				fmt.Println(alphax[tag])
				resultx = resultx + alphax[tag]
			}
			return resultx
		}
	}

	//NUMERIC BY DEFAULT
	resultx := ""
	length = 6
	for x := int64(0); x < length; x++ {
		tag := int64(rand.Intn(10))
		resultx = resultx + nums[tag]
	}
	return resultx
}
