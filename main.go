package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/prosp3r/benchr/benchr"
	//"gitlab.com/prosp3r/benchr"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:    1024,
	WriteBufferSize:   1024,
	EnableCompression: true,
}

func main() {

	//PRELOAD FROM DB FOR SPEED
	//
	//jsonword := blog.FormatErrorMessage("This is the message")
	//fmt.Println(jsonword)
	//guidx := blog.GetGUID(12, "ALPHANUMERIC")
	//fmt.Println(guidx)
	//server := make(chan error)
	//go benchr.Benchmark()
	benchr.Benchmark()

	//usage place this line at the start of each function you'll like to measure
	//defer bnechr.measureTime("funcname")()
	//TEST
	for counter := int64(0); counter <= 1000; counter++ {
		//fmt.Printf("\n Run GetMainGUID number: %v =>", counter)
		_ = GetMainGUID(counter, "ALPHANUMERIC")
		//fmt.Println(gnum)
	}

}




func measureTime(funcName string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("Time taken by %s function is %v \n", funcName, time.Since(start))
	}
}

//GetMainGUID Returns a random (alphabetic, alphanumeric, numeric) generated string of given length
func GetMainGUID(length int64, resultType string) string {
	defer benchr.MeasureTime("GetMainGUID")()
	//defer measureTime("GetMainGUID")()

	alphax := make([]string, 0)
	alphax = append(alphax, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

	nums := make([]string, 0)
	nums = append(nums, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9")
	//PREVENT SAME NUMBER GENERATION ERROR WITH RAND.INTN
	rand.Seed(time.Now().UnixNano())

	if length > 0 {
		if resultType == "ALPHANUMERIC" {
			resultx := ""

			alphaNum := append(nums, alphax...) //join both alphabetic and numeric slices
			//fmt.Println(alphaNum)
			for x := int64(0); x < length; x++ {
				maxi := len(alphaNum) //maximum length of new slice
				tag := int64(rand.Intn(maxi))
				resultx = resultx + alphaNum[tag]
			}
			return resultx
		}

		if resultType == "NUMERIC" {
			resultx := ""
			for x := int64(0); x < length; x++ {
				tag := int64(rand.Intn(10))
				resultx = resultx + nums[tag]
			}
			return resultx
		}

		if resultType == "ALPHABETIC" {
			resultx := ""
			for x := int64(0); x < length; x++ {
				tag := int64(rand.Intn(10))
				fmt.Println(alphax[tag])
				resultx = resultx + alphax[tag]
			}
			return resultx
		}
	}

	//NUMERIC BY DEFAULT
	resultx := ""
	length = 6
	for x := int64(0); x < length; x++ {
		tag := int64(rand.Intn(10))
		resultx = resultx + nums[tag]
	}
	return resultx
}
