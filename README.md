**BENCHR**

**A light weight Go function benchmarking module**

**Version 1.0**

* Measure the time each function takes to execute (helps to spot bottleneck functions due to latency)
* User should be able to use benchr in any go project by just calling the benchr function.
* User should be able to see functions being called and their run times in real time on the browser/graph

#
**What Technologies will be needed for Benchr to work**
1. Go(Golang)
2. A WebSocket enabled browser(Mozilla Firefox, Google Chrome)
3. Browser based animated graphing tools(css/javascript)
4. Javascript(Vanilla/JQuery)

#
**How this will be used**

1. Imports module by adding

 `import "https://gitlab.com/prosp3r/benchr"`
#
2. Only if you have to, edit configuration file in benchrConf.json if you would like to:
   *  Specify where you want your logfiles stored if different from /var/log
   *  Specify which port you want benchr to run on if different from 2019

#
3. Call the benchr function in your function calls(all the functions you'll like to track).
    e.g:

    `benchr.Benchmark() //run this on func main(){} to starts the webserver for displaying benchmark on the browser`


	`//usage place this line at the start of each function you'll like to measure`


	`defer benchr.measureTime("funcname")()`    

#
4. For monitoring i.e. seeing how your functions are performing in a visual graph, go into the benchr folder and run:

    `go build -i -v -o ./`
    
    
    `./benchr`

#
5. visit http://localhost:2019 on browser that supports websockets(chrome, firefox, etc) 
   watch how each function in your running project are performing

#
*This is an evolving document...* 
*Benchr project is minimalist and written for Go Programmers(+me), any Go Programmer can use and pitch in with feedbacks.*

*Benchr is inspired by Arindam Roy's Post on: Easy Guide to Latency Measurement, & My obsession with performance & My new found Love for playing with WebSockets and Go*
*https://bit.ly/36j80nf*